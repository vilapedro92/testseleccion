const redis = require('redis');
let client = redis.createClient();
const bcrypt = require('bcrypt-node');

const createUsers = require('../models/createUsersModels');

const createUserCtrl = {};

createUserCtrl.createUser = (req, res) => {

    let user_name = req.body.user_name;

    client.hgetall(req.body.user_name, (err, obj) => {
        if(!obj){
            bcrypt.hash(req.body.password, null, null, (error,hash) => {

                if(error){
                    console.log(`Error ${error}`);
                }
                else{
                    console.log(hash);
                    client.hmset(user_name, [ 
                        'user_name', user_name,
                        'password', hash
                    ],
                    (err, replay) => {
                        if(err){
                            console.log(`Error creating user ${err}`);
                            res.status(500).json({message: `Error creating user, error: ${err}`});            
                        }
                        else{
                            console.log(`Status ${replay}`);
                            res.status(201).json({message: 'User created succesfull'});
                        }
                    })  
                }
            }); 
        }
        else{
            if(obj.user_name == user_name){
                return res.status(409).json({message: 'User exists'});
            }
        }        
        
    });         
    
}

module.exports = createUserCtrl;


