const redis = require('redis');
let client = redis.createClient();
const axios = require('axios');
const https = require('https');

// temporal con los datos del api para probar.
const dataAPI = require('../models/createUsersModels')


const getAPICtrl = {};
const characterCtrl = {};

getAPICtrl.getFromAPI = async (req, res) => {
        
        https.get('https://rickandmortyapi.com/api/character/ ', (resp) =>{
           
           let characters = '';
           
           resp.on('data', (chunk) => {
               characters += chunk;                    
           });

           resp.on('end', () => {

               const JSONcharacters = JSON.parse(characters);  
               console.log(JSONcharacters);
               characterCtrl.characters = createResponse(JSONcharacters);
               res.status(200).json({characterCtrl});                  
           });

                      
       }).on('error', (e) => {
        res.status(500).json({message: 'Error in server'});
        });     
    
}

//names, status, species, gender and image.
function createResponse(JSONcharacters){
    let arrayData = [];
    for(let i=0; i<JSONcharacters.results.length; i++){
        let tempo = {};
        tempo.name = JSONcharacters.results[i].name;
        tempo.status = JSONcharacters.results[i].status;
        tempo.species = JSONcharacters.results[i].species;
        tempo.gender = JSONcharacters.results[i].gender;
        tempo.image = JSONcharacters.results[i].image;
        console.log(tempo)
        arrayData.push(tempo);
    }
    return arrayData;
}

module.exports = getAPICtrl;
