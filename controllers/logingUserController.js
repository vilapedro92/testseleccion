const redis = require('redis');
let client = redis.createClient();
const bcrypt = require('bcrypt-node');
const jwt = require('jsonwebtoken');

const logingUserCtrl = {};

logingUserCtrl.logingUser = async (req, res) => {

    // var username = req.body.user_name; 
    // var password = req.body.password;
    var username = req.body.user; 
    var password = req.body.pass;

    client.hgetall(username, (err, obj) => {
        if(!obj){
            return res.status(404).json({message: 'User does not exist' });
        }
        else{
            let hash = obj.password;
            bcrypt.compare(password, hash, function(err, resp) {
                if(err){
                    return res.status(401).json({message: 'Auth failed'});
                }
                else{
                    if(resp){
                        const token = jwt.sign({
                            user_name: username
                        },
                        "JWT_SECRET_KEY",
                        {
                            expiresIn: 60 * 2
                        }
                        );
                        return res.status(200).json({
                            message: 'Auth successful',
                            token: token
                        });
                    }
                    else{
                        return res.status(401).json({message: 'Auth failed'});
                    }
                    
                }
            });   
        }
    });
    
}

module.exports = logingUserCtrl;


