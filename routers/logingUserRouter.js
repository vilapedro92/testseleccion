const express = require('express');
const router = express.Router();
const logingUsersControllers = require('../controllers/logingUserController');

router.post('/', logingUsersControllers.logingUser);

module.exports = router;