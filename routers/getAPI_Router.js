const express = require('express');
const router = express.Router();
const getAPIControllers = require('../controllers/getAPI_Controller');
const checkAuth = require('../middleware/check-auth');

router.get('/', checkAuth, getAPIControllers.getFromAPI);

module.exports = router;