const express = require('express');
const router = express.Router();
const createUsersControllers = require('../controllers/createUsersController');

router.post('/', createUsersControllers.createUser);

module.exports = router;