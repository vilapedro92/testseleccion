const express = require('express');
const app = express();

const cors = require('cors');
const redis = require('redis');
let client = redis.createClient();

app.get('/', (req,res) => {
    res.send('API REST NodeJS like backend server')
});

//Settings
app.set('port', process.env.PORT || 3000);
const createUsers = require('./routers/createUsersRouter');
const logingUsers = require('./routers/logingUserRouter');
const getFromAPI = require('./routers/getAPI_Router');

//Middlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(cors());


client.on('connect', ()=>{
    console.log('Connection to Readis');
});

//Routes
app.use('/api/signup', createUsers);//create user
app.use('/api/login', logingUsers);//login user
app.use('/api/characters', getFromAPI);//get api

// Starting the Server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${process.env.PORT} or 3000`);
});